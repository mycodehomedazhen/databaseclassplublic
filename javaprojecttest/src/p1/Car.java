package p1;

public class Car {

    private int doors;
    private int wheels;
    private String model;
    private String engine;
    private String colour;

    public Car(){
        System.out.println("You just created a new car");
    }

    public void setModel(String model){
        this.model= model;
    }

    public String getModel(){
        return this.model;
    }
}
