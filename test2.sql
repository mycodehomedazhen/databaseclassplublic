DROP VIEW IF EXISTS ResultWithPercentage CASCADE;

-- Prepare the view
-- CREATE VIEW ResultWithPercentage AS
-- SELECT r.id, r.election_id, r.party_id, alliance_id, r.seats, r.votes, e.country_id, e_date, votes_valid, r.votes/votes_valid::numeric as vote_percentile
-- FROM election_result r, election e
-- WHERE r.election_id=e.id AND
-- r.votes is NOT NULL;


-- -- Prepare the view
-- CREATE VIEW ResultWithPercentage AS
-- SELECT r.id, r.election_id, r.party_id, r.votes, e.country_id, e_date, extract(year from e_date) as year, votes_valid, r.votes/votes_valid::numeric as vote_percentile
-- FROM election_result r, election e
-- WHERE r.election_id=e.id AND
-- r.votes is NOT NULL;



-- SELECT *
-- FROM ResultWithPercentage
-- WHERE election_id = 628;


-- SELECT * FROM election WHERE id=628;

-- Prepare the view
CREATE VIEW ResultWithPercentage AS
SELECT r.id, r.election_id, p.name as partyName, r.votes, c.name as countryName, e_date, extract(year from e_date) as year, votes_valid, r.votes/votes_valid::numeric as vote_percentile
FROM election_result r, election e, country c, party p
WHERE r.election_id=e.id AND
e.country_id=c.id AND
r.party_id=p.id AND
r.votes is NOT NULL;

CREATE VIEW ResultwithYearAverage AS
SELECT election_id, partyName, countryName, year, avg(vote_percentile)
FROM ResultWithPercentage
GROUP BY election_id, partyName, countryName, year;


SELECT * FROM ResultwithYearAverage LIMIT 20;