
public class Main {


    public static void main(String[] args){
    	String url="jdbc:postgresql://localhost:5432/testjava";
    	String username="postgres";
    	String password="root";

    	try {
    		Assignment2 a2 = new Assignment2();
    		boolean c = a2.connectDB(url, username, password);
    		if(c) {
    			System.out.println("db connected");
    		}else {
    			System.out.println("cannot connect to db");
    		}

    	} catch (ClassNotFoundException e) {
    		System.out.println("class not found");
    	}

    }
}
