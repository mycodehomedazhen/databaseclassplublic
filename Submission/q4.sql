-- Left-right

SET SEARCH_PATH TO parlgov;

drop table if exists q4 cascade;
DROP VIEW IF EXISTS partyLeftRightRange, rangeWithCountry, countryRangeCount CASCADE;

CREATE TABLE q4(
        countryName VARCHAR(50),
        r0_2 INT,
        r2_4 INT,
        r4_6 INT,
        r6_8 INT,
        r8_10 INT
);


CREATE VIEW partyLeftRightRange AS

(SELECT party_id, left_right, 'r0_2' as range
FROM party_position
WHERE left_right>=0 AND left_right<2)

UNION

(SELECT party_id, left_right, 'r2_4' as range
FROM party_position
WHERE left_right>=2 AND left_right<4)

UNION

(SELECT party_id, left_right, 'r4_6' as range
FROM party_position
WHERE left_right>=4 AND left_right<6)

UNION

(SELECT party_id, left_right, 'r6_8' as range
FROM party_position
WHERE left_right>=6 AND left_right<8)

UNION

(SELECT party_id, left_right, 'r8_10' as range
FROM party_position
WHERE left_right>=8 AND left_right<=10);


CREATE VIEW rangeWithCountry AS
SELECT c.name as countryName, party_id, range
FROM partyLeftRightRange r, party p, country c
WHERE r.party_id=p.id AND p.country_id=c.id;


CREATE VIEW countryRangeCount AS
SELECT countryName, range, COUNT(party_id) as countryRangeCount
FROM rangeWithCountry
GROUP BY countryName, range
ORDER BY countryName, range ASC;



INSERT INTO q4(countryName, r0_2, r2_4, r4_6, r6_8, r8_10)
SELECT name as countryName, 0 as r0_2, 0 as r2_4, 0 as r4_6, 0 as r6_8, 0 as r8_10
FROM country;

UPDATE q4
SET r0_2=c.countryRangeCount
FROM countryRangeCount c
WHERE q4.countryName=c.countryName AND
c.range='r0_2';

UPDATE q4
SET r2_4=c.countryRangeCount
FROM countryRangeCount c
WHERE q4.countryName=c.countryName AND
c.range='r2_4';

UPDATE q4
SET r4_6=c.countryRangeCount
FROM countryRangeCount c
WHERE q4.countryName=c.countryName AND
c.range='r4_6';

UPDATE q4
SET r6_8=c.countryRangeCount
FROM countryRangeCount c
WHERE q4.countryName=c.countryName AND
c.range='r6_8';

UPDATE q4
SET r8_10=c.countryRangeCount
FROM countryRangeCount c
WHERE q4.countryName=c.countryName AND
c.range='r8_10';