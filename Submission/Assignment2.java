import java.sql.*;
import java.util.List;
import java.util.*;

// If you are looking for Java data structures, these are highly useful.
// Remember that an important part of your mark is for doing as much in SQL (not Java) as you can.
// Solutions that use only or mostly Java will not receive a high mark.
//import java.util.ArrayList;
//import java.util.Map;
//import java.util.HashMap;
//import java.util.Set;
//import java.util.HashSet;
public class Assignment2 extends JDBCSubmission {


    public Assignment2() throws ClassNotFoundException {

        Class.forName("org.postgresql.Driver");
    }

    @Override
    public boolean connectDB(String url, String username, String password) {
        // Implement this method!
        try {
            this.connection = DriverManager.getConnection(url, username, password);
            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return false;
    }

    @Override
    public boolean disconnectDB() {
        // Implement this method!
        if(this.connection!=null){
            try {
                this.connection.close();
                return true;

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

        return false;
    }

    @Override
    public ElectionCabinetResult electionSequence(String countryName) {
        // Implement this method!
        List<Integer> elections = new ArrayList<Integer>();
        List<Integer> cabinets = new ArrayList<Integer>();
        
        System.out.println("Running test");
        
        try{
            Statement st = this.connection.createStatement();
            st.executeUpdate("SET SEARCH_PATH TO parlgov");
            
            String query = "SELECT election_id, cabinet.id as cabinet_id FROM cabinet, country WHERE cabinet.country_id=country.id AND country.name= ? ORDER BY start_date DESC";
            
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, countryName);
            
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()) {
                elections.add(rs.getInt("election_id"));
                cabinets.add(rs.getInt("cabinet_id"));
            }
            
        }catch(Exception e){
            System.out.println("error occured");
            System.out.println(e.getMessage());
        }
        
        return new ElectionCabinetResult(elections, cabinets);
    }

    @Override
    public List<Integer> findSimilarPoliticians(Integer politicianName, Float threshold) {
        // Implement this method!
        List<Integer> politicians = new ArrayList<Integer>();
        
        String check_description = "";
        
        try{
            Statement st = this.connection.createStatement();
            st.executeUpdate("SET SEARCH_PATH TO parlgov");
            
            //Get the description of the input politician
            String query = "SELECT CONCAT(description, comment) AS description FROM politician_president WHERE id= ? ";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, politicianName);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()) {
                check_description = rs.getString("description"); 
            }
            
            //Get a list of all politician description
            String query_all_descriptions = "SELECT id, CONCAT(description, comment) AS description FROM politician_president";
            ResultSet rs_alldescripiton = st.executeQuery(query_all_descriptions);

            while(rs_alldescripiton.next()) {
                double sim_ratio = this.similarity(rs_alldescripiton.getString("description"), check_description);
                if(sim_ratio >= threshold) {
                    int id = rs_alldescripiton.getInt("id");
                    if(id != politicianName){
                        politicians.add(id);
                    }
                }
            }
                
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        return politicians;
    }

    public static void main(String[] args) {
        // You can put testing code in here. It will not affect our autotester.
        System.out.println("Hello");
    }

}

