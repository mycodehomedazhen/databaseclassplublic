-- Participate

SET SEARCH_PATH TO parlgov;

drop table if exists q3 cascade;

-- You must not change this table definition.

create table q3(
        countryName varchar(50),
        year int,
        participationRatio real
);

-- You may find it convenient to do this for each of the views
-- that define your intermediate steps.  (But give them better names!)
-- Define views for your intermediate steps here.


-- the answer to the query

DROP VIEW IF EXISTS electionWithRatio, yearAverageElectionRatio,qualifiedCountries, disqualifiedCountries, allCountriesInYearRange, answer CASCADE;


CREATE VIEW electionWithRatio AS
SELECT id, country_id, e_date, extract(year from e_date) as electionYear, electorate, votes_valid, votes_cast/electorate::numeric as participationRatio
FROM election
WHERE electorate is NOT NULL;


CREATE VIEW yearAverageElectionRatio AS
SELECT country_id, electionYear, avg(participationRatio) as participationRatio
FROM electionWithRatio
WHERE electionYear>=2001 AND electionYear<=2016
GROUP BY country_id, electionYear;


CREATE VIEW disqualifiedCountries AS
SELECT DISTINCT e1.country_id
FROM yearAverageElectionRatio e1, yearAverageElectionRatio e2
WHERE e1.country_id = e2.country_id AND
e1.electionYear < e2.electionYear AND
e1.participationRatio > e2.participationRatio;


CREATE VIEW allCountriesInYearRange AS
SELECT DISTINCT country_id
FROM yearAverageElectionRatio;

CREATE VIEW qualifiedCountries AS
(SELECT * FROM allCountriesInYearRange)
EXCEPT
(SELECT * FROM disqualifiedCountries);


CREATE VIEW answer AS
SELECT c.name as countryName, electionYear as year, participationRatio
FROM yearAverageElectionRatio e, qualifiedCountries q, country c
WHERE e.country_id=q.country_id AND
e.country_id=c.id;


insert into q3(countryName, year, participationRatio)
SELECT countryName, year, participationRatio
FROM answer;