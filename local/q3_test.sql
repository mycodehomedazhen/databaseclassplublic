
DROP VIEW IF EXISTS germanyElection, test CASCADE;

drop table if exists q3 cascade;

-- You must not change this table definition.

create table q3(
        countryName varchar(50),
        year int,
        participationRatio real
);



-- CREATE VIEW germanyElection AS
-- SELECT e.id, e_date, extract(year from e_date) AS year, c.name as countryname, electorate, votes_cast
-- FROM election e, country c
-- WHERE e.country_id=c.id;

-- CREATE VIEW test AS
-- SELECT *
-- FROM germanyElection;


insert into q3(countryName, year, participationRatio)
SELECT CONCAT(id, '/', country_id, '/', e_date, '/', seats_total, '/', votes_cast) AS countryName, electorate, votes_valid
FROM election;

-- SELECT * FROM q3 LIMIT 20;

-- SELECT id, country_id, e_date, seats_total, electorate, votes_cast, votes_valid, e_type
-- FROM election LIMIT 20;