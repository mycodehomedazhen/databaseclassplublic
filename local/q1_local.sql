DROP VIEW IF EXISTS ResultWithPercentage CASCADE;
DROP VIEW IF EXISTS ResultwithYearAverage CASCADE;
DROP VIEW IF EXISTS ResultwithVoteRange CASCADE;
drop table if exists q1 cascade;

create table q1(
year INT,
countryName VARCHAR(50),
voteRange VARCHAR(20),
partyName VARCHAR(100)
);


-- Prepare the view
CREATE VIEW ResultWithPercentage AS
SELECT r.id, r.election_id, p.name_short as partyName, r.votes, c.name as countryName, e_date, extract(year from e_date) as year, votes_valid, r.votes/votes_valid::numeric as vote_percentile
FROM election_result r, election e, country c, party p
WHERE r.election_id=e.id AND
e.country_id=c.id AND
r.party_id=p.id AND
r.votes is NOT NULL;

CREATE VIEW ResultwithYearAverage AS
SELECT year, partyName, countryName, avg(vote_percentile) as vote_percentile
FROM ResultWithPercentage
WHERE year<=2016 AND year>=1996
GROUP BY partyName, countryName, year;


-- Assign voteRange string
CREATE VIEW ResultwithVoteRange AS
(SELECT year, partyName, countryName, vote_percentile, '(0-5]' as voteRange
FROM ResultwithYearAverage
WHERE vote_percentile<=0.05)

UNION

(SELECT year, partyName, countryName, vote_percentile, '(5-10]' as voteRange
FROM ResultwithYearAverage
WHERE vote_percentile>0.05 AND vote_percentile<=0.10)

UNION

(SELECT year, partyName, countryName, vote_percentile, '(10-20]' as voteRange
FROM ResultwithYearAverage
WHERE vote_percentile>0.10 AND vote_percentile<=0.20)

UNION

(SELECT year, partyName, countryName, vote_percentile, '(20-30]' as voteRange
FROM ResultwithYearAverage
WHERE vote_percentile>0.20 AND vote_percentile<=0.30)

UNION

(SELECT year, partyName, countryName, vote_percentile, '(30-40]' as voteRange
FROM ResultwithYearAverage
WHERE vote_percentile>0.30 AND vote_percentile<=0.40)

UNION

(SELECT year, partyName, countryName, vote_percentile, '(40-100]' as voteRange
FROM ResultwithYearAverage
WHERE vote_percentile>0.40);

-- Insert final view into table
insert into q1 (year, countryName, partyName, voteRange)
Select year, countryName, partyName, voteRange
From ResultwithVoteRange;