DROP VIEW IF EXISTS electionMaxVotes CASCADE;
DROP VIEW IF EXISTS electionWinner CASCADE;
DROP VIEW IF EXISTS winnerWithCountry CASCADE;
DROP VIEW IF EXISTS winCount CASCADE;
DROP VIEW IF EXISTS winAvgCountry CASCADE;
DROP VIEW IF EXISTS qualifyParty CASCADE;
DROP VIEW IF EXISTS partyMostRecnetWinDate CASCADE;
DROP VIEW IF EXISTS partyMostRecnetWinElection CASCADE;
DROP VIEW IF EXISTS answerWithoutPartyFamily CASCADE;
DROP VIEW IF EXISTS answer CASCADE;


CREATE VIEW electionMaxVotes AS
SELECT election_id, max(votes) as maxVotes
FROM election_result
GROUP BY election_id;

CREATE VIEW electionWinner AS
SELECT r.election_id, r.party_id
FROM election_result r, electionMaxVotes
WHERE r.votes=electionMaxVotes.maxVotes;

CREATE VIEW winnerWithCountry AS
SELECT w.election_id, w.party_id, e_date, c.name as countryName
FROM electionWinner w, election e, country c
WHERE w.election_id=e.id AND
e.country_id=c.id;

CREATE VIEW winCount AS
SELECT count(election_id) as wonElections, party_id, countryName
FROM winnerWithCountry
GROUP BY party_id, countryName;

CREATE VIEW winAvgCountry AS
SELECT avg(wonElections) as avgWon, countryName
FROM winCount
GROUP BY countryName;

CREATE VIEW qualifyParty AS
SELECT wonElections, party_id, c.countryName
FROM winCount as c, winAvgCountry as a
WHERE c.countryName=a.countryName AND
c.wonElections> (avgWon*3);

CREATE VIEW partyMostRecnetWinDate AS
SELECT max(e_date) as mostRecentWonDate, party_id
FROM winnerWithCountry
GROUP BY party_id;

CREATE VIEW partyMostRecnetWinElection AS
SELECT w.election_id, w.party_id, e_date, extract(year from e_date) as mostRecentlyWonElectionYear
FROM winnerWithCountry w, partyMostRecnetWinDate d
WHERE w.party_id=d.party_id AND
w.e_date=d.mostRecentWonDate;

CREATE VIEW answerWithoutPartyFamily AS
SELECT q.countryname, q.party_id, p.name as partyName, wonElections, rw.election_id as mostRecentlyWonElectionId, rw.mostRecentlyWonElectionYear
FROM qualifyParty q, party p, partyMostRecnetWinElection as rw
WHERE q.party_id=p.id AND
q.party_id=rw.party_id;

CREATE VIEW answer AS
SELECT countryname, partyName, wonElections, mostRecentlyWonElectionId, mostRecentlyWonElectionYear, family
FROM answerWithoutPartyFamily a
LEFT JOIN party_family on a.party_id=party_family.party_id;

SELECT * FROM answer;