
DROP VIEW IF EXISTS partyLeftRightRange CASCADE;

CREATE VIEW partyLeftRightRange AS

(SELECT party_id, left_right, 'r0_2' as range
FROM party_position
WHERE left_right<2)

UNION

(SELECT party_id, left_right, 'r2_4' as range
FROM party_position
WHERE left_right>=2 AND left_right<4)

UNION

(SELECT party_id, left_right, 'r4_6' as range
FROM party_position
WHERE left_right>=4 AND left_right<6)

UNION

(SELECT party_id, left_right, 'r6_8' as range
FROM party_position
WHERE left_right>=6 AND left_right<8)

UNION

(SELECT party_id, left_right, 'r8_10' as range
FROM party_position
WHERE left_right>=8 AND left_right<=10);


CREATE VIEW rangeWithCountry AS
SELECT c.name as countryName, party_id, range
FROM partyLeftRightRange r, party p, country c
WHERE r.party_id=p.id AND p.country_id=c.id;


CREATE VIEW countryRangeCount AS
SELECT countryName, range, COUNT(party_id)
FROM rangeWithCountry
GROUP BY countryName, range
ORDER BY countryName, range ASC;


-- SELECT * FROM countryRangeCount LIMIT 20;